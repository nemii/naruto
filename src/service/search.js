import axios from 'axios';

export const searchNaruto = async (e, limit) => {
    try {
        const response = await axios.get(`https://api.jikan.moe/v3/search/anime?q=${e}&limit=${limit}`);
        if(response) {
            return { success: true, data: response };
        }
        return { success: false, data: [] };
    } catch(e) {
        console.log(e);
        return { success: false, data: [] }
    }
}