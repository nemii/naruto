import React from 'react';
import styled from "styled-components";
import { selectSearch } from "../features/user/searchSlice";
import Header from './Header';
import { Link } from "react-router-dom";
import { searchNaruto } from '../service/search';
import { useDispatch, useSelector } from "react-redux";
import { setResult, setDetail } from '../features/user/searchSlice';

const Search = (props) => {
  const result = useSelector(selectSearch);
  const dispatch = useDispatch();
  const [search, setSearch] = React.useState('');
  const [limit, setLimit] = React.useState(16);

  const handleSubmit = async (e, localLimit) => {
    if (search) {
      const response = await searchNaruto(search, localLimit ? localLimit : limit);
      if (response && response.success) {
        dispatch(
          setResult({
            result: response.data.data.results
          })
        );
      } else {
        alert('No result found!');
      }
    }
  }

  const loadMore = () => {
    setLimit(limit + 16);
    handleSubmit(limit + 16);
  }

  const saveDetail = (detail) => {
    dispatch(
      setDetail({
        detail: detail
      })
    );
  }

  return (
    <>
      <Header handleSubmit={handleSubmit} search={search} setSearch={setSearch} />
      <Container>
        {result && result.length > 0 && result.map(v => <Wrap key={v.mal_id} onClick={()=>saveDetail(v)}>
          <Link to={`/detail/${v.mal_id}`}>
            <ImageWrap><img src={v.image_url} alt="" /></ImageWrap>
            <TitleWrap>{v.title}</TitleWrap>
          </Link>
        </Wrap>
        )}

        {result && result.length <= 0 && <a>No Data Found!</a>}
      </Container>
      {result && result.length > 0 && <ButtonContainer onClick={loadMore}>
        <button>Load More</button>
      </ButtonContainer>}
    </>
  );
};

const Container = styled.div`
  margin-top: 60px;
  padding: 30px 0px 26px;
  display: grid;
  grid-gap: 25px;
  gap: 25px;
  grid-template-columns: repeat(5, minmax(0, 1fr));

  @media (max-width: 768px) {
    grid-template-columns: repeat(1, minmax(0, 1fr));
  }
`;

const ButtonContainer = styled.div`
  width:100%;
  text-align: center;
  padding: 20px;
  margin-bottom: 20px;
  button {
    border: none;
    background: blue;
    border-radius: 5px;
    color: white;
    padding: 10px;
    cursor: pointer;
  }
`;

const TitleWrap = styled.div`
  padding-top: 10px;
  padding-bottom: 10px;
  padding-left:10px;
  // background: grey;
  color: white;
`;

const Wrap = styled.div`
  border-radius: 10px;
  box-shadow: rgb(0 0 0 / 69%) 0px 26px 30px -10px,
    rgb(0 0 0 / 73%) 0px 16px 10px -10px;
  cursor: pointer;
  transition: all 250ms cubic-bezier(0.25, 0.46, 0.45, 0.94) 0s;
  border: 3px solid rgba(249, 249, 249, 0.1);
  display: table-cell;
  margin-right: 20px;

  &:hover {
    box-shadow: rgb(0 0 0 / 80%) 0px 40px 58px -16px,
      rgb(0 0 0 / 72%) 0px 30px 22px -10px;

    transform: scale(1.05);
    border-color: rgba(249, 249, 249, 0.8);
  }
`;

const ImageWrap = styled.div`
img {
  inset: 0px;
  display: block;
  height: 30vh;
  object-fit: cover;
  opacity: 1;
  transition: opacity 500ms ease-in-out 0s;
  width: 100%;
  @media (max-width: 768px) {
    height: 30vh;
  }
}


`;

export default Search;
