import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import searchReducer from "../features/user/searchSlice";

export default configureStore({
  reducer: {
    search: searchReducer
  },
  middleware: getDefaultMiddleware({
    serializableCheck: false,
  }),
});
