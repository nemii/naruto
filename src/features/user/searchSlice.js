import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  result: [],
  detail: null
};

const searchSlice = createSlice({
  name: "search",
  initialState,
  reducers: {
    setResult: (state, action) => {
      state.result = action.payload.result;
    },
    setDetail: (state, action) => {
      state.detail = action.payload.detail;
    }
  },
});

export const { setResult, setDetail } = searchSlice.actions;

export const selectSearch = (state) => state.search.result;
export const selectSearchById = (state) => state.search.detail;

export default searchSlice.reducer;
